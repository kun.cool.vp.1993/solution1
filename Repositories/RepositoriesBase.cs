using Datalayers.Context;
using Microsoft.Extensions.Logging;

namespace Repositories;

public abstract class RepositoriesBase<T>
{
    public readonly DataContext _context;
    protected readonly ILogger<T> _logger;

    public RepositoriesBase(DataContext context, ILogger<T> logger)
    {
        _context = context;
        _logger = logger;
    }
}