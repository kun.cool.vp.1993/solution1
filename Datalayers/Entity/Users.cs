using Microsoft.AspNetCore.Identity;

namespace Datalayers.Entity;

public class Users : IdentityUser<Guid>
{
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public bool IsActive { get; set; }
    public bool IsDeleted { get; set; }
    
}