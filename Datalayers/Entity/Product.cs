namespace Datalayers.Entity;

public class Product :EntityBase
{
    public Guid CategoryId { get; set; }
    public string ProductName { get; set; }
    public Category Category { get; set; }
}