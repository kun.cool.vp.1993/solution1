using Microsoft.AspNetCore.Identity;

namespace Datalayers.Entity;

public class Role : IdentityRole<Guid>
{
    public bool IsActive { get; set; }
    public bool IsDeleted { get; set; }
}