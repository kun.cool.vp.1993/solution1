namespace Datalayers.Entity;

public class Category :EntityBase
{
    public string CategoryName { get; set; }
    public Guid CategoryId { get; set; }
    public Product Product { get; set; }
}