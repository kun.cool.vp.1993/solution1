using Newtonsoft.Json;
namespace Models.Request;

public class TableSearchModel
{
    public int Draw { get; set; }
    public int Start { get; set; }
    public int Length { get; set; }
    public int OrderColumn { get; set; }
    public int OrderDir { get; set; }
    public string SearchValue { get; set; }

    public T Get<T>()
    {
        T obj = default(T);
        try
        {
            string rs = JsonConvert.SerializeObject(this);
            obj = JsonConvert.DeserializeObject<T>(rs);
        }
        catch (Exception ex)
        {
            return obj;
        }
        return obj;
    }
}