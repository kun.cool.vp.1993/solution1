using Microsoft.AspNetCore.Http;
using Models.Request;

namespace Models.Respone;

public class TableRespone<T>
{
    public TableRespone()
    {
        Code = StatusCodes.Status200OK;
        RecordsTotal = RecordsFiltered = 0;
        Data = new List<T>();
    }

    public  int Code { get; set; }
    
    public bool Success { get; set; }
    
    public string Message { get; set; }
    
    public string DataError { get; set; }
    public int Draw { get; set; }
    public int RecordsTotal { get; set; }
    public int RecordsFiltered { get; set; }
    public List<T> Data { get; set; }

    public void setDraw(TableSearchModel search)
    {
        Draw = search.Draw;
    }
}