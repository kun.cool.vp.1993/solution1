using Microsoft.AspNetCore.Http;

namespace Models.Respone;

public class Respone<T>
{
    public Respone()
    {
        Code = StatusCodes.Status200OK;
    }
    public int Code { get; set; }
    public string Message { get; set; }
    public T Data { get; set; }
    public List<T> DataList { get; set; }
}